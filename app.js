document.addEventListener('DOMContentLoaded', () => {
	const grid = document.querySelector('.grid');
	const miniGrid = document.querySelector('.mini-grid');
	const height = 200;
// Draw divs with for loops
	for (let i=0; i<height; i++) {
		let divs = document.createElement("div");
		grid.appendChild(divs);
	}

	for (let i=0; i<10; i++) {
		let taken = document.createElement("div");
		taken.className="taken";
		grid.appendChild(taken)
	}

	for (let i=0; i<16; i++) {
		let mini=document.createElement("div");
		miniGrid.appendChild(mini);
	}

// declaring variables

	let squares = Array.from(document.querySelectorAll('.grid div'));
	const scoreDisplay = document.querySelector('#score');
	const status = document.querySelector('#status');
	const startBtn = document.querySelector('#start-button');
						// or: document.getElementById('start-button')
	const againBtn = document.getElementById('again');

	const width = 10;
	let nextRandom = 0;
	let timerId;
	let score = 0;
	const colors = [
	'orange',
	'red',
	'purple',
	'green',
	'blue'
	];


	// The Tetrominoes
	const lTetromino = [
		[1, width+1, width*2+1, 2],
		[width, width+1, width+2, width*2+2],
		[1, width+1, width*2+1, width*2],
		[width, width*2, width*2+1, width*2+2]
	];

	const zTetromino = [
		[0, width, width+1, width*2+1],
		[width+1, width+2, width*2, width*2+1],
		[0, width, width+1, width*2+1],
		[width+1, width+2, width*2, width*2+1]
	];

	const tTetromino = [
		[1, width, width+1, width+2],
		[1, width+1, width+2, width*2+1],
		[width, width+1, width+2, width*2+1],
		[1, width, width+1, width*2+1]
	];

	const oTetromino = [
		[0,1, width, width+1],
		[0,1, width, width+1],
		[0,1, width, width+1],
		[0,1, width, width+1]
	];

	const iTetromino = [
		[1, width+1, width*2+1, width*3+1],
		[width, width+1, width+2, width+3],
		[1, width+1, width*2+1, width*3+1],
		[width, width+1, width+2, width+3]
	];

	let tetrominoes = [lTetromino, zTetromino, tTetromino, oTetromino, iTetromino];

	// we choose starting field of the Tetrominoes
	let currentPosition = 4;
	let currentRotation = 0; //add variable to always start with the first rotation

	//randomly select a Tetromino and its first rotation
	let random = Math.floor(Math.random()*tetrominoes.length);
	

	// modify current to choose a random Tetromino and have variable for starting rotation
	let current = tetrominoes[random][currentRotation];
	//draw the Tetromino
	function draw() {
		if (current == null) {
			return;
		} else {
			current.forEach(index => {
				squares[currentPosition + index].classList.add('tetromino');
				//we are using .classList.add() to add a class 'tetromino' to each index of the current array
				//console.log('current', current);
				squares[currentPosition + index].style.backgroundColor = colors[random];
			})
		}
	}

	// add functionality to start/pause button
	startBtn.addEventListener('click', () => {
		if (timerId) {
			clearInterval(timerId);
			timerId = null;
		} else {
			draw();
			timerId = setInterval(moveDown, 1000);
			nextRandom = Math.floor(Math.random()*tetrominoes.length);
			displayShape();
		}
	})

//MINI GRID
	// show up-next tetromino in mini-grid display
	const miniSquares = document.querySelectorAll('.mini-grid div'); //alternative to Array.from
	const miniWidth = 4;
	const miniIndex = 0;


	// the Tetrominoes without rotations
	const upNextTetrominoes = [
		[1, miniWidth+1, miniWidth*2+1, 2], //lTetromino
		[0, miniWidth, miniWidth+1, miniWidth*2+1], //zTetromino
		[1, miniWidth, miniWidth+1, miniWidth+2], //tTetromino
		[0, 1, miniWidth, miniWidth+1],	//oTetromino
		[1, miniWidth+1, miniWidth*2+1, miniWidth*3+1] //iTetromino
	];

	// display the shape in the mini-grid
	function displayShape() {
		// remove any trace of a tetromino from the entire grid
		if (!miniSquares) {
			return;
		} else {
			miniSquares.forEach(square => {
			square.classList.remove('tetromino');
			square.style.backgroundColor = '';
			})
			upNextTetrominoes[nextRandom].forEach( index => {
				miniSquares[miniIndex + index].classList.add('tetromino');
				miniSquares[miniIndex + index].style.backgroundColor = colors[nextRandom];
			})
		}
	}
	//undraw the Tetromino
	function undraw() {
		if (current == null) {
			return;
		} else {
			current.forEach(index => {
				squares[currentPosition + index].classList.remove('tetromino');
				squares[currentPosition + index].style.backgroundColor = '';
			})
		}
	}

	//make the tetromino move down every second
	//timerId = setInterval(moveDown, 1000) // moved to start/pause button

	// assign functions to keyCodes
	function movement(e) {
		if(e.keyCode === 37) {
			moveLeft();
		} else if (e.keyCode === 38) {
			// rotate
			rotate();
		} else if (e.keyCode === 39) {
			// move right
			moveRight();
		} else if (e.keyCode === 40) {
			//move down faster
			moveDown();
		}
	}
	document.addEventListener('keyup', movement);

	//move down function
	function moveDown() {
		undraw();
		currentPosition += width;
		draw();
		// add freeze here
		freeze();
	}

	//freeze function
	function freeze() {
		if (current == null) {
			return;
		} else if(current.some(index => squares[currentPosition + index + width].classList.contains('taken'))) {
			current.forEach(index => squares[currentPosition + index].classList.add('taken'))
			// start a new tetromino falling
			random = nextRandom;
			nextRandom = Math.floor(Math.random() * tetrominoes.length);
			current = tetrominoes[random][currentRotation];
			currentPosition = 4;
			draw();
			displayShape();
			addScore();
			gameOver();
		}
	}
	
	// move the tetromino left, unless it is at the edge or there is a blockage
	function moveLeft() {
		undraw();
		const isAtLeftEdge = current.some(index => (currentPosition + index) % width === 0);
		if (!isAtLeftEdge) {
			currentPosition -=1;
		}
		if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
			currentPosition +=1;
		}	
		draw();
	}

	//move the tetromino right, unless it is at the right edge or there is a blockage
	function moveRight() {
		undraw();
		const isAtRightEdge = current.some(index =>(currentPosition + index) % width === width -1);
		if(!isAtRightEdge) {
			currentPosition +=1;
		}
		if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
			currentPosition -=1;
		}
		draw();
	}

///FIX ROTATION OF TETROMINOS AT THE EDGE 
  function isAtRight() {
    return current.some(index=> (currentPosition + index + 1) % width === 0)  
  }
  
  function isAtLeft() {
    return current.some(index=> (currentPosition + index) % width === 0)
  }
  
  function checkRotatedPosition(P){
    P = P || currentPosition  
   // console.log('p is', P);     //get current position.  Then, check if the piece is near the left side.
    if ((P+1) % width < 4) {         //add 1 because the position index can be 1 less than where the piece is (with how they are indexed).     
      if (isAtRight()){            //use actual position to check if it's flipped over to right side
        currentPosition += 1; 
                                 //if so, add one to wrap it back around
        checkRotatedPosition(P); //check again.  Pass position from start, since long block might need to move more.
       // console.log('current', P); 
        }
    }
    else if (P % width > 5) {
      if (isAtLeft()){
        currentPosition -= 1;
      checkRotatedPosition(P);
      }
    }
  }


	// rotate the tetromino i.e. go to next rotation
	function rotate() {
		undraw();
		currentRotation ++;
		// if the current rotation gets to 4, make it go back to 0
		if(currentRotation === current.length) {
			currentRotation = 0;
		} 
		current = tetrominoes[random][currentRotation];
		checkRotatedPosition();
		draw();
	}

	// add score
	function addScore() {
		for (let i=0; i<199; i+=width) {
			const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9];
			if(row.every(index => squares[index].classList.contains('taken'))) {
				score +=10;
				scoreDisplay.innerHTML = score;
				row.forEach(index => {
					squares[index].classList.remove('taken');
					squares[index].classList.remove('tetromino');
					squares[index].style.backgroundColor = '';
				})
				const squaresRemoved = squares.splice(i, width);
				//console.log(squaresRemoved);
				squares = squaresRemoved.concat(squares);
				//console.log('squares :', squares)
				squares.forEach(cell => grid.appendChild(cell));
			}
		}
	}

	//game over
	function gameOver () {
		//console.log(typeof(squares))
		if (current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
			//alert('Game Over'); //works
			//console.log (currentPosition)
			status.innerHTML = 'Game Over';
			//status.style.backgroundColor = 'red';
			status.style.color='red';
			clearInterval(timerId);
			timerId=null;
			current = null;
		}
	}

// PLAY AGAIN BUTTON
	againBtn.addEventListener('click', () => {
		document.location.reload(true);
	})
})